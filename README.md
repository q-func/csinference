# Code for the paper: "Large-Scale Correlation Screening under Dependence for Brain Functional Connectivity Network Inference"
### Authors: Hanâ Lbath, Alexander Petersen, Sophie Achard

This repository contains the code used in the paper titled "Large-Scale Correlation Screening under Dependence for Brain Functional Connectivity Network Inference".

The notebook Source_code_list.html contains links to the codes used to generate the figures and the content of the tables of the paper.
